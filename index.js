const express = require('express')
const app = express()
const port = 8000

app.use("/", express.static("public"))
app.set("view engine", "ejs")
app.use(express.urlencoded({extended: true}))


app.get("/login", (req, res) => {
    let error = {
        message: req.query.error || "Please login!"
    }
    res.render("login", error)
})

app.post("/login", (req, res) => {
    // logic ..
    try {
        let { username, password } = req.body

        if  (username == "nineclout" && password == "777clout") {
            res.redirect("/the-games/games.html")
            return
        }
    } catch (e) {
        console.log(e.message)
    }
    res.redirect("/login?error=invalid%20username")
})

app.listen(port, () => console.log(`listening at http://localhost:${port}`))