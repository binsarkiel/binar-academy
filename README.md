## 🚀 Follow these steps to run the app

1. Clone the repository 
```bash
git clone https://github.com/binsarkiel/binar-academy.git
```
2. Open the directory
3. Rename the file `.env.example` to `.env`. Or you can use this command
```bash
# GitBash or Terminal on Linux machine
mv .env.example .env

# Command Prompt on Windows machine
ren .env.example .env
```
4. Run this command to install all required packages based on **packages.json**
```bash
npm install 
```
5. Check the database configuration on **config/config.json**, and then run this command
```bash
sequelize db:create
```
6. Run this command to migrate the tables based on **models** and **migrations** directory files
```bash
sequelize db:migrate
```
7. Run this command to seed all the tables based on **seeders** directory files
```bash
sequelize db:seed:all
```
8. Refresh and check **PostgreSQL** database if it successfully made a change
9. Run this command to enable the **server**
```bash
npm start
```
